﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Author: Dylan Tyrie-Dron
 * 
 * Modified on: 27/10/16
 * 
 * This class gves us the attributes to our attendee and checks for user errors at the same time
 * to ensure all the data is correct. It also calculates the cost of the ticket due to the type
 * of registration the user selects.
 */

namespace cw1_cs
{
    class Attendee
    {
        //all private variables declared
        private int attendeeref;
        private string institutionname;
        private string conferencename;
        private string registrationtype;
        private bool paid;
        private bool presenter;
        private string papertitle;
        private string firstname;
        private string secondname;

        // public variable for the attendee's first name stored as a string, 
        // the field is checked if the user has not entered
        // any characters and if not then an error is thrown
        public string FirstName
        {
            get
            {
                return firstname;
            }

            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("you havent entered your first name, please enter your first name");
                }
                firstname = value;
            }
        }

        // public variable for the attendee's second name stored as a string, 
        // the field is checked if the user has not entered
        // any characters and if not then an error is thrown
        public string SecondName
        {
            get
            {
                return secondname;
            }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("you havent entered your first name, please enter your first name");
                }
                secondname = value;
            }
        }

        // public variable for the attendee's ref number stored as a int, 
        // the field is checked if the user has entered a number between
        // a certain range and if not an error is thrown
        public int AttendeeRef
        {
            get
            {
                return attendeeref;
            }
            set
            {
                if(value < 40000 || value > 60000)
                {
                    throw new ArgumentException("the ref number is not in the range: 40000 - 60000");
                } 
                attendeeref = value;
            }
        }

        // public variable for the attendee's institution name stored as a string
        public string InstitutionName
        {
            get
            {
                return institutionname;
            }
            set
            {
                institutionname = value;
            }
        }

        // public variable for the attendee's conference name stored as a string, 
        // the field is checked if the user has not entered
        // any characters and if not then an error is thrown
        public string ConferenceName
        {
            get
            {
                return conferencename;
            }
            set
            {
                if(String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("you havent entered the name of your conference, please enter your first name");
                }
                conferencename = value;
            }
        }

        // public variable for the attendee's reg type stored as a string, 
        // the field is checked if the user has entered the right string
        //  and if not then an error is thrown
        public string RegistrationType
        {
            get
            {
                return registrationtype;
            }
            set
            {
                if (value.Trim().Equals("full") || value.Trim().Equals("student") || value.Trim().Equals("organiser"))
                {
                    registrationtype = value;                    
                } 
                else
                {
                    throw new ArgumentException("you have entered the an unofficial reg type, please enter a valid reg type");
                }               
            }
        }
        // gets and sets the value paid
        public bool Paid
        {
            get
            {
                return paid;
            }
            set
            {
                paid = value;
            }
        }

        // gets and sets the value presenter
        public bool Presenter
        {
            get
            {
                return presenter;
            }
            set
            {
                presenter = value;
            }
        }

        // public variable for the presenter's paper title stored as a string, 
        // the field is checked if the user has not entered
        // any characters and if not then an error is thrown
        public string PaperTitle
        {
            get
            {
                return papertitle;
            }
            set
            {
                if((Presenter && String.IsNullOrWhiteSpace(value)) || (!Presenter && !String.IsNullOrWhiteSpace(value)))
                {
                    throw new ArgumentException("if you are a presenter then please enter the title of your paper, if not untick the rpesenter option and leave this field blank");
                }
                papertitle = value;
            }
        }

        //calculates the costs based on the reg type by using a a variable to store the data 
        public double getCost()
        {
            double cost = 0;

            if (RegistrationType.Trim().Equals("full"))
            {
                cost = 500;
            }
            else if(RegistrationType.Trim().Equals("student"))
            {
                cost = 300;
            }
            else
            {
                cost = 0;
            }

            if(Presenter)
            {
                return cost - (cost/ 10);
            }
            return cost;
        }
    }
}
