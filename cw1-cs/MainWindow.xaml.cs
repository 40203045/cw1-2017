﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/*
* Author: Dylan Tyrie-Dron
* 
* Modified: 27/10/2016
* 
* This class runs the main code in the program, it checks all the fields in the attendee gui for
* errors and it also performs actions like bringing up windows from button clicks in the gui.
*/
namespace cw1_cs
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //new attendee declared publicly
        Attendee attendee1 = new Attendee();
        private void set_btn_Click(object sender, RoutedEventArgs e)
        {
            //test each error by checking whats in each field in the gui and throwing an errors if any are found
            try
            {
                attendee1.AttendeeRef = Int32.Parse(attendref_txtbx.Text);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                attendee1.FirstName = firstname_txtbx.Text;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                attendee1.SecondName = secondname_txtbx.Text;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

            attendee1.InstitutionName = instname_txtbx.Text;

            try
            {
                attendee1.ConferenceName = confname_txtbx.Text;

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                attendee1.RegistrationType = regtype_txtbx.Text;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

            attendee1.Paid = false;
            if (paid_cbox.IsChecked.Value)
            {
                attendee1.Paid = true;
            }

            attendee1.Presenter = false;
            if (presenter_cbox.IsChecked.Value)
            {
                attendee1.Presenter = true;
            }

            try
            {
                attendee1.PaperTitle = papertitle_txtbx.Text;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        //clears everything and sets the checkboxes to zero so that everything is blank
        private void clear_btn_Click(object sender, RoutedEventArgs e)
        {
            
            firstname_txtbx.Clear();
            secondname_txtbx.Clear();
            attendref_txtbx.Clear();
            instname_txtbx.Clear();
            confname_txtbx.Clear();
            regtype_txtbx.Clear();
            papertitle_txtbx.Clear();
            paid_cbox.IsChecked = false;
            presenter_cbox.IsChecked = false;
        }

        //takes the values from all the attendee variables and stores the values in each textbox
        //the values from the checkboxes are also taken and set 
        private void get_btn_Click(object sender, RoutedEventArgs e)
        {
     
            firstname_txtbx.Text = attendee1.FirstName;
            secondname_txtbx.Text = attendee1.SecondName;
            attendref_txtbx.Text = Convert.ToString(attendee1.AttendeeRef);
            instname_txtbx.Text = attendee1.InstitutionName;
            confname_txtbx.Text = attendee1.ConferenceName;
            regtype_txtbx.Text = attendee1.RegistrationType;
            papertitle_txtbx.Text = attendee1.PaperTitle;
            if(attendee1.Paid == true)
            {
                paid_cbox.IsChecked = true;
            } 
            else
            {
                paid_cbox.IsChecked = false;
            }

            if (attendee1.Presenter == true)
            {
                presenter_cbox.IsChecked = true;
            }
            else
            {
                presenter_cbox.IsChecked = false;
            }
        }

        //on invoice button click open invoice window 
        private void invoice_btn_Click(object sender, RoutedEventArgs e)
        {
            //create new window
            Invoice InvoiceWin = new Invoice();

            //create invoice window and when found, add all the values 
            //stored in the variables of the attendee class to the textboxes in the gui
            foreach (Window window in Application.Current.Windows)
            {
                if (window.GetType() == typeof(Invoice))
                {
                    
                    (window as Invoice).name_txtbx.Text= (attendee1.FirstName + " " + attendee1.SecondName);
                    (window as Invoice).inst_txtbx.Text= (attendee1.InstitutionName);
                    (window as Invoice).conf_txtbx.Text= (attendee1.ConferenceName);
                    (window as Invoice).price_txtbx.Text= ("£" + attendee1.getCost());
                }
            }
            //show window that can be closed without shutting the program down
            InvoiceWin.ShowDialog();
        }

        //create certificate window and when found, add all the values 
        //stored in the variables of the attendee class to the textboxes in the gui
        private void certificate_btn_Click(object sender, RoutedEventArgs e)
        {
            //create a new window
            Certificate certWin = new Certificate();

            //create invoice window and when found, add all the values 
            //stored in the variables of the attendee class to the textboxes in the gui
            foreach (Window window in Application.Current.Windows)
            {
                if (window.GetType() == typeof(Certificate))
                {
                    // (window as Certificate).lbox_cert.Items.Add("This is to certify that " + attendee1.FirstName + " " + attendee1.SecondName + " attended " + attendee1.ConferenceName);
                    (window as Certificate).fname_txtbx.Text = attendee1.FirstName;
                    (window as Certificate).sname_txtbx.Text = attendee1.SecondName;
                    (window as Certificate).confcertify_txtbx.Text = attendee1.ConferenceName;
                    if (attendee1.Presenter)
                    {
                       //show hidden textboxes and put papertitle in one of them
                       (window as Certificate).papertitlecertify_txtbx.Visibility= System.Windows.Visibility.Visible;
                       (window as Certificate).paptitle_txtbx.Visibility = System.Windows.Visibility.Visible;
                       (window as Certificate).paptitle_txtbx.Text = attendee1.PaperTitle;
                    }
                }
            }
            //show window that can be closed without shutting the program down
            certWin.ShowDialog();
        }
    }
}
